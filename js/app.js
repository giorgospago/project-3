var app = angular.module("project3", []);

app.controller("MainController", function($scope,$http){

    $scope.products = [];
    $scope.search = {};
    $scope.loader = false;
    $scope.suppliers = [];

    $scope.getProducts = function(){

        $scope.loader = true;

        $http.get("https://iek.develobird.gr/products.json")
            .then(function(response){
                $scope.products = response.data;
                $scope.loader = false;

                for(var i = 0; i < $scope.products.length; i++){
                    if($scope.suppliers.indexOf($scope.products[i].supplier) == -1){
                        $scope.suppliers.push($scope.products[i].supplier);
                    }
                }

                console.log($scope.suppliers);

            });
    };

    $scope.one_product = false;
    $scope.product = {};

    $scope.showProduct = function(temp_product){
        $scope.one_product = true;
        $scope.product = temp_product;
    };

});